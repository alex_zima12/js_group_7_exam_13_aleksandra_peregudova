const express = require('express');
const upload = require("../middleware/upload");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const Gallery = require('../models/Images');

const router = express.Router();

router.get('/', async (req, res) => {
    const gallery = await Gallery.find().populate('place');
    for(let image of gallery) {
        const images = await Gallery.find({place: gallery._id});
        gallery.photosCount = images.length;
    }
    res.send(gallery)
});

router.post('/new/:id', [upload.single('image'), auth], async (req, res) => {
    const galleryData = req.body;

    if (req.file) {
            galleryData.image = req.file.filename;
        }

        const gallery = new Gallery({
            user: req.user._id,
            image: galleryData.image,
            place: req.params.id
        });

        try {
            await gallery.save();

            res.send({message: `Успешно опубликовано`});
        } catch (e) {
            console.log(e);
            res.status(500).send(e);
        }

});

router.delete(
    "/:id",
    [auth, permit("admin")],
    async (req, res) => {
        try {
            const gallery = await Gallery.findById(req.params.id);
            if(!gallery) return res.status(404).send({error: 'Фото не существует'});
            await Gallery.deleteOne({_id: req.params.id});
            return res.send({message: `Фото было удалено`});
        } catch (e) {
            res.status(500).send(e);
        }
    }
);


module.exports = router;

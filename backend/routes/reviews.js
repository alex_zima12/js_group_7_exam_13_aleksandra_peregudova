const router = require('express').Router();
const permit = require("../middleware/permit");
const auth = require('../middleware/auth');
const ReviewsModel = require('../models/Reviews');

router.get('/:id', async (req, res) => {

    let reviews;
    try {
        reviews = await ReviewsModel
            .find({place: req.params.id})
            .sort({date: -1})
            .populate('user')
        if(!reviews || reviews.length === 0) return res.status(404).send({error: 'Отзывов для данного заведения нет. Оставьте первый отзыв'});
        return res.send(reviews);
    } catch (e) {
        res.status(500);
    }
});

router.post(
    "/:id",
    [auth],
    async (req, res) => {
        try {
            const review = new ReviewsModel({
                text: req.body.text,
                place: req.body.place,
                user: req.user._id,
                quality: req.body.rating.quality,
                service: req.body.rating.service,
                interior:req.body.rating.interior
            });
            await review.save();

            return res.send({message: "Спасибо за Ваш отзыв!"});
        } catch (e) {
            res.status(500);
        }
    }
);

router.delete('/:id', [auth, permit("admin")], async (req, res) => {
    try {
        const review = await ReviewsModel.findById(req.params.id);
        if(!review) return res.status(404).send({error: 'Такой отзыв не существует'});
        await ReviewsModel.deleteOne({_id: req.params.id});
        return res.send({message: `Отзыв был удален`});
    } catch (e) {
        res.status(500);
    }
});

module.exports = router;

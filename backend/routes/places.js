const express = require('express');
const upload = require("../middleware/upload");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const Places = require('../models/Place');
const Reviews = require('../models/Reviews');

const router = express.Router();

router.get('/', async (req, res) => {
    const places = await Places.find();
    for(let place of places) {
        const reviews = await Reviews.find({place: place._id});
        place.reviewsCount = reviews.length;
    }
    res.send(places)
});

router.get('/:id', async (req, res) => {
    try {
        const place = await Places.findById(req.params.id).populate('user')
        if (place.length === 0) return res.status(404).send({error: 'Заведения не добавлены'});
        return res.send(place);
    } catch (e) {
        res.status(500);
    }
});

router.post('/new', [upload.single('image'), auth], async (req, res) => {
    const placeData = req.body;

    if (placeData.agree) {
        if (req.file) {
            placeData.image = req.file.filename;
        }

        const place = new Places({
            user: req.user._id,
            name: placeData.name,
            description: placeData.description,
            image: placeData.image,
        });

        try {
            await place.save();
            res.send({message: `Успешно опубликовано`});
        } catch (e) {
            res.status(422).send(e);
        }
    } else res.send({message: `Подтвердите для отправки`});

});

router.delete(
    "/:id",
    [auth, permit("admin")],
    async (req, res) => {
        try {
            const place = await Places.findById(req.params.id);
            if(!place) return res.status(404).send({error: 'Такого заведения не существует'});
            await Places.deleteOne({_id: req.params.id});
            return res.send({message: `Заведение было удалено`});
        } catch (e) {
            res.status(500).send(e);
        }
    }
);


module.exports = router;

const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');

const User = require('./models/User');
const Place = require('./models/Place');

mongoose.connect(config.database, config.databaseOpt);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        const collections = await mongoose.connection.db.listCollections().toArray();
        for(let collItem of collections) {
            await db.dropCollection(collItem.name);
        }
        const [user, admin] = await User.create({
            username: 'user',
            password: 'testpass',
            token: nanoid(),
            displayName: 'USER'
        }, {
            username: 'admin',
            password: 'testpass',
            token: nanoid(),
            displayName: 'ADMIN'
        });
        const place = await Place.create(
            {
                user: user,
                name: 'Bellagio',
                description: 'Приглашаем наших гостей заглянуть в Bellagio ☕️ попробовать десерты от наших кондитеров и выпить ароматный, итальянский кофе 🤤😍',
                image:'photo1.jpg',
                agree: true
            },
            // {
            //     user: admin,
            //     name: 'Кафе',
            //     description: 'На протяжении многих лет мы создаем для наших клиентов атмосферу уюта и отдыха. Каждый день нас посещает огромное количество людей всех возрастов, начиная от студентов и заканчивая деловыми людьми. Всех их объединяет одно -  любовь к восточной кухне.  "Фаиза" - сеть кафе, заслужившая известность далеко за пределами страны, благодаря превосходной кухне, первоклассному обслуживанию и вниманию к каждому гостю.',
            //     image:'faiza.jpeg',
            //     agree: true
            // },
        );

    } catch (e) {
        console.log(e);
    }
    await db.close();
});
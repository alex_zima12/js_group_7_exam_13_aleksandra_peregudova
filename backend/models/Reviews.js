const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReviewSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true,
    },
    place: {
        type: Schema.Types.ObjectId,
        ref: 'Place',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    date: {
        type: String,
        default: new Date().toISOString()
    },
    quality: {
        type: Number,

    },
    service: {
        type: Number,

    },
    interior:{
        type: Number,

    }
});

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;

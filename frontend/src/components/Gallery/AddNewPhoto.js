import React, {useState} from 'react';
import {Container} from "@material-ui/core";
import {postNewPhoto} from "../../store/actions/galleyActions";
import {useDispatch, useSelector} from "react-redux";
import AddNewPhotoForm from "./AddNewPhotoForm";

const AddNewPhoto = ({id}) => {
    const dispatch = useDispatch();
    const user = useSelector((state) => state.users.user);

    const [state, setState] = useState({
        image: ""
    });

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach((key) => {
            formData.append(key, state[key]);
        });
        dispatch(
            postNewPhoto(
                formData, id
            )
        );
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    return (
        <Container>
            {user &&
            <AddNewPhotoForm
                fileChangeHandler={fileChangeHandler}
                user={user}
                submitFormHandler={submitFormHandler}
            />}

        </Container>
    );
};

export default AddNewPhoto;
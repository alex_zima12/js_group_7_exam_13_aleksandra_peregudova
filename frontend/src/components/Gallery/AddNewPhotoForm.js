import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import {
    Button,
    Typography,
} from "@material-ui/core";
import FileInput from "../UI/FormElement/FileInput";

const useStyles = makeStyles((theme) => ({
    form: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    photo: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2)
    },
    btn: {
        margin: '0 0 0 auto',
        display: 'block'
    }
}));


const AddNewPhotoForm = ({
                             submitFormHandler,
                             user,
                             fileChangeHandler
                         }) => {
    const classes = useStyles();


    return (
        user && (
            <>
                <Typography
                    variant="h5"
                    align="center"
                >
                    Добавьте новое фото в галлерею
                </Typography>
                <form
                    className={classes.form}
                    autoComplete="off"
                    onSubmit={submitFormHandler}
                >

                    <FormControl
                        fullWidth
                        className={classes.photo}
                        variant="outlined"
                    >
                        <FileInput
                            label={"image"}
                            name="image"
                            id="place-image"
                            onChange={fileChangeHandler}
                        />
                    </FormControl>

                    <Button
                        type="submit"
                        variant="contained"
                        className={classes.btn}
                    >
                        Отправить
                    </Button>

                </form>
            </>
        )
    );

};

export default AddNewPhotoForm;
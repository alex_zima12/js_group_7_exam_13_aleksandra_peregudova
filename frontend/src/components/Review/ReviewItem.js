import React from 'react';
import {makeStyles, Typography, Paper} from "@material-ui/core";
import Moment from "react-moment";
import IconButton from "@material-ui/core/IconButton";
import {deleteReview} from "../../store/actions/reviewsAction";
import DeleteIcon from "@material-ui/icons/Delete";
import {useDispatch, useSelector} from "react-redux";
import PropTypes from "prop-types";

const useStyles = makeStyles(theme => ({
    container: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(1)
    },
    author: {
        fontWeight: 'bold',
        paddingLeft: theme.spacing(1)
    },
    btn: {
        margin: '0 0 0 auto',
        display: 'block'
    }
}));

const ReviewItem = ({id, text, author, date, quality, service, interior}) => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);

    let deleteBtn = <Paper elevation={0}>
        <IconButton
            id='deleteReview'
            onClick={() => dispatch(deleteReview(id))}
            className={classes.btn}
        >
            <DeleteIcon
                fontSize="large"
            />
        </IconButton>
    </Paper>;

    return (
        <>
            <Paper
                elevation={3}
                className={classes.container}
            >
                <Paper elevation={0} className={classes.container}>
                    <Typography
                    >
                        <Moment format="DD.MM.YYYY HH:mm">{date}</Moment>
                        <Typography
                            variant="overline"
                            className={classes.author}
                        >
                            {author}
                        </Typography>
                    </Typography>
                </Paper>
                <Paper elevation={0}>
                    <Typography
                    >
                        {text}
                    </Typography>
                    <Typography
                    >
                        quality={quality}
                    </Typography>
                    <Typography
                    >
                        service={service}
                    </Typography>
                    <Typography
                    >
                        interior={interior}
                    </Typography>
                </Paper>
                {user && user.role === "admin" ?
                    deleteBtn
                    : null}
            </Paper>
        </>
    );
};

ReviewItem.propTypes = {
    id: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    author: PropTypes.object.isRequired,
    date: PropTypes.string.isRequired
};

export default ReviewItem;

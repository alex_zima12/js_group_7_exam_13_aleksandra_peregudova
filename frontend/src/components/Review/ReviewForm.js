import React from 'react';
import PropTypes from 'prop-types';
import {makeStyles, TextField, Button, IconButton} from '@material-ui/core';
import Picker from "emoji-picker-react";
import InsertEmoticonIcon from "@material-ui/icons/InsertEmoticon";
import FormElement from "../UI/FormElement/FormElement";

const useStyles = makeStyles(theme => ({
    form: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
    },
    review: {
        marginBottom: theme.spacing(2),
        marginTop: theme.spacing(2)
    },
    btn: {
        margin: '0 0 0 auto',
        display: 'block'
    },
    select: {
        display: 'flex',
    },
}));

const ReviewForm = ({text, change, getFieldError, submit,isDisabled,click,emoji,setEmoji,inputChangeHandler,rating}) => {
    const classes = useStyles();

    let options = [1,2,3,4,5]

    return (
        <form
            className={classes.form}
            autoComplete="off"
            onSubmit={submit}
        >
            <TextField
                id='review'
                label={"Оставьте Ваш отзыв"}
                name="text"
                value={text}
                type="text"
                required
                error={!!getFieldError('text')}
                helperText={getFieldError('text')}
                onChange={change}
                fullWidth
                multiline
                rows={3}
                className={classes.review}
                variant={"outlined"}
            />

            <IconButton
                type='button'
                onClick={() => setEmoji(!emoji)}
            >
                <InsertEmoticonIcon />
            </IconButton>
            <div className={classes.select}>
                <FormElement
                    name="quality"
                    label={"quality"}
                    required
                    select={true}
                    options={options}
                    value={rating.quality}
                    onChange={inputChangeHandler}
                />

                <FormElement
                    name="service"
                    label={"service"}
                    required
                    select={true}
                    options={options}
                    value={rating.service}
                    onChange={inputChangeHandler}
                />

                <FormElement
                    name="interior"
                    label={"interior"}
                    required
                    select={true}
                    options={options}
                    value={rating.interior}
                    onChange={inputChangeHandler}
                />
            </div>

            <Button
                type="submit"
                variant="contained"
                className={classes.btn}
                disabled={isDisabled}
            >
       Отправить
            </Button>
            {emoji && <Picker
                onEmojiClick={click}
                pickerStyle={{
                    position: "absolute",
                    marginTop:"120px"
                }}
            />}
        </form>
    );
};

ReviewForm.propTypes = {
    change: PropTypes.func.isRequired,
    getFieldError: PropTypes.func.isRequired,
    submit: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};

export default ReviewForm;

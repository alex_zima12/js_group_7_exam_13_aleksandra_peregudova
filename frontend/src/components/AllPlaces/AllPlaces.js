import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid, Typography} from "@material-ui/core";
import {getPlaces} from "../../store/actions/placesActions";
import PlaceItem from "../Place/PlaceItem";
import Alert from '@material-ui/lab/Alert';

const AllPlaces = () => {
    const {places, placesErrors} = useSelector(state => state.places);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getPlaces());
    }, [dispatch]);

    return (
        <Grid>
            <Container>
                <Typography
                    align="center"
                    variant="h4"
                    component="h2"
                >
                    Зведения
                </Typography>
                {!placesErrors && places.length !== 0 ?
                    <Grid container>
                        {places && places.map(place => {
                                return <PlaceItem
                                    key={place._id}
                                    id={place._id}
                                    name={place.name}
                                    description={place.description}
                                    image={place.image}
                                    count={place.reviewsCount}
                                />
                        })}

                    </Grid> : <Alert severity="info">Нет добавленых заведений</Alert>
                }
            </Container>
        </Grid>

    );
};

export default AllPlaces;

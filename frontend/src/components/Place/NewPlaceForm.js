import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import {
    Button,
    Checkbox,
    FormControlLabel,
    Typography,
} from "@material-ui/core";
import FileInput from "../UI/FormElement/FileInput";
import FormElement from "../UI/FormElement/FormElement";


const useStyles = makeStyles((theme) => ({
    root: {
        "& > *": {
            marginTop: theme.spacing(3),
            width: "100%",
        },
    },
}));


const NewPlaceForm = ({
                          user,
                          submitFormHandler,
                          state,
                          inputChangeHandler,
                          getFieldError,
                          agree,
                          checkboxChangeHandler,
                          fileChangeHandler,
                          warning

                      }) => {
    const classes = useStyles();


    return (
        user && (
            <>
                <Typography component="h1" variant="h5">
                    Форма добавления нового заведения
                </Typography>
                <form
                    className={classes.root}
                    autoComplete="off"
                    onSubmit={submitFormHandler}
                >

                    <FormElement
                        name="name"
                        label={"name"}
                        required
                        value={state.name}
                        id="place-name"
                        onChange={inputChangeHandler}
                        error={getFieldError('name')}
                    />
                    <FormElement
                        name="description"
                        label={"description"}
                        required
                        value={state.description}
                        id="place-description"
                        onChange={inputChangeHandler}
                        error={getFieldError('description')}
                    />
                    <FormControl
                        fullWidth
                        className={classes.margin}
                        variant="outlined"
                    >
                        <FileInput
                            label={"image"}
                            name="image"
                            id="place-image"
                            onChange={fileChangeHandler}
                        />
                    </FormControl>

                    <FormControlLabel
                        control={
                            <Checkbox
                                value={agree}
                                checked={agree}
                                onChange={checkboxChangeHandler}
                                name="agree"
                                id="place-agree"
                                color={"primary"}
                            />
                        }
                        label={"agree"}
                    />
                    {warning}
                    <Button
                        type="submit"
                        color="primary"
                        id="btn-send"
                    >
                        Отправить
                    </Button>

                </form>
            </>
        )
    );

};

export default NewPlaceForm;
import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getFullPlaceInfo} from "../../store/actions/placesActions";
import {Grid, makeStyles, Typography} from "@material-ui/core";
import defaultImage from "../../assets/images/default.png";
import {apiUrl} from "../../constants";
import Reviews from "../../containers/Reviews/Reviews";
import AddNewPhoto from "../Gallery/AddNewPhoto";
import Gallery from "../../containers/Gallery/Gallery";

const useStyles = makeStyles(theme => ({

    item: {
        width: 'auto',
        boxShadow: '0 0 40px 1px rgba(255, 255, 255, 0.2)',
        padding: theme.spacing(2),
        marginBottom: theme.spacing(2),
        color: '#000',
    },
    itemTitle: {
        borderBottom: '1px solid rgba(0, 0, 0, 0.5)',
        paddingBottom: theme.spacing(4),
        marginBottom: theme.spacing(4),
        fontSize: '30px'
    },
    fullPageImage: {
        width: '100%',
        height: 'auto',
        display: 'block',
        objectFit: 'cover',
        marginBottom: '30px'
    },
    gridImg: {
        maxWidth: '500px',
        margin: '0 auto'
    }
}));


const PlacePage = props => {
    const classes = useStyles();
    const {place, placesErrors} = useSelector(state => state.places);
    const dispatch = useDispatch();
    const id = props.match.params.id;

    let cardImage = defaultImage;
    if (place && place.image) {
        cardImage = apiUrl + "/uploads/" + place.image;
    }


    useEffect(() => {
        dispatch(getFullPlaceInfo(id));
    }, [dispatch, id, props]);

    return place && (
        <Grid>
            {!placesErrors ?
                <Grid>
                    <Grid
                        item
                        className={classes.item}
                    >
                        {cardImage &&
                        <Grid
                            className={classes.gridImg}
                        >
                            <img
                                src={cardImage}
                                alt={place.name}
                                className={classes.fullPageImage}
                            />
                        </Grid>}
                        <Typography
                            variant='h3'
                            align='center'
                            className={classes.itemTitle}
                        >
                            {place.name}

                        </Typography>
                        <Typography>
                            {place.description}
                        </Typography>
                    </Grid>
                    <Gallery
                        id={id}
                    />
                    <AddNewPhoto
                        id={id}
                    />
                    <Reviews
                        id={id}
                    />
                </Grid> : <p>{placesErrors}</p>}
        </Grid>
    );
};

export default PlacePage;

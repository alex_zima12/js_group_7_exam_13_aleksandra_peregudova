import React from "react";
import {
    Grid,
    Card,
    makeStyles,
    CardContent,
    Button,
    Badge,
} from "@material-ui/core";
import RateReviewIcon from '@material-ui/icons/RateReview';
import DeleteForeverIcon from "@material-ui/icons/DeleteForever";
import IconButton from "@material-ui/core/IconButton";
import { NavLink } from "react-router-dom";
import {deletePlace} from "../../store/actions/placesActions";
import { useDispatch, useSelector } from "react-redux";
import defaultImage from "../../assets/images/default.png";
import {apiUrl} from "../../constants";

const useStyles = makeStyles((theme) => ({
    root: {
        position: "relative",
        borderRadius: '10px'
    },
    titleLink: {
        display: "block",
        margin: "0 auto",
        textAlign: "center",
    },
    badgeBox: {
        position: "absolute",
        bottom: "10px",
        right: "20px",

    },
    contentContainer: {
        display: 'flex',
        justifyContent: 'space-between',
        borderRadius: '10px',
        flexDirection: 'column',
        alignItems: 'center',
    },
    contentItem: {
        width: '80%',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        '& .MuiCardHeader-root': {
            alignItems: 'unset'
        }
    },
    imageBox: {
        width: '100%',
        minWidth: '225px',
        [theme.breakpoints.up('sm')]: {
            width: '20%'
        }
    },
    placeImage: {
        width: '100%',
        height: 'auto',
        objectFit: 'cover',
        display: 'block'
    },
    btns: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    placeHeader: {
        flexGrow: 1
    },
    item: {
        margin: '20px',
    }
}));

const PlaceItem = ({ id, name, count, image }) => {
    const classes = useStyles();
    const user = useSelector((state) => state.users.user);
    const dispatch = useDispatch();

    let cardImage = defaultImage;
    if (image) {
        cardImage =  apiUrl + "/uploads/" + image;
    }

    return (
        <Grid lg={3} item className={classes.item}>
            <Card className={classes.root}>
                <Grid className={classes.contentContainer}>
                    <Grid className={classes.imageBox}>
                        <img
                            src={cardImage}
                            alt={name}
                            className={classes.placeImage}
                        />
                    </Grid>
                </Grid>
                <Grid className={classes.contentItem}>
                    <CardContent>
                        <div className={classes.badgeBox}>
                            <Badge
                                badgeContent={count}
                                color="primary"
                                showZero
                            >
                                <RateReviewIcon/>
                            </Badge>
                        </div>
                        <Button
                            component={NavLink}
                            to={`/places/${id}`}
                            color="inherit"
                            className={classes.titleLink}
                        >
                            {name}
                        </Button>
                        {user && user.role === "admin" ? (
                            <Grid className={classes.btns}>
                                <IconButton
                                    color="secondary"
                                    onClick={() => dispatch(deletePlace(id))}
                                >
                                    <DeleteForeverIcon/>
                                </IconButton>
                            </Grid>
                        ) : null}
                    </CardContent>
                </Grid>
            </Card>
        </Grid>

    );};

export default PlaceItem;
import React from 'react';
import {apiUrl} from "../../constants";
import {makeStyles} from "@material-ui/core/styles";
import GridListTile from '@material-ui/core/GridListTile';


const useStyles = makeStyles((theme) => ({
    photo: {
     height: "200px"
    },
}));

const PhotoItem = ({image}) => {
    const classes = useStyles();
    let cardImage;
    if (image) {
        cardImage =  apiUrl + "/uploads/" + image;
    }
    return (
    <GridListTile >
        <img className={classes.photo} src={cardImage} alt={"Картинка галлереи"} />
    </GridListTile>
    );
};

export default PhotoItem;
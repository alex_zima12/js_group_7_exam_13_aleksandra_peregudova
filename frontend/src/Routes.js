import React from 'react';
import {Switch, Route, Redirect} from 'react-router-dom';
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Layout from "./components/Layout/Layout";
import LoginRegister from "./containers/LoginRegister";
import Main from "./containers/Main/Main";
import CreateNewPlace from "./containers/CreateNewPlace/CreateNewPlace";
import PlacePage from "./components/Place/PlacePage";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ?
        <Route {...props} /> :
        <Redirect to={redirectTo} />
};

const Routes = ({user}) => {
    return (
        <Layout>
            <ToastContainer autoClose={3000} />
            <Switch>
                <Route path='/' exact component={Main} />
                <ProtectedRoute
                    isAllowed={!user}
                    path='/login'
                    exact
                    component={LoginRegister}
                    redirectTo='/'
                />
                <ProtectedRoute
                    isAllowed={!user}
                    path='/register'
                    exact
                    component={LoginRegister}
                    redirectTo='/'
                />
                <ProtectedRoute
                    isAllowed={user}
                    path='/create-new-place'
                    exact
                    component={CreateNewPlace}
                    redirectTo='/'
                />
                <Route path='/places/:id' exact component={PlacePage} />
                <Route render={() => <h1 style={{textAlign: 'center'}}>404 Page Not Found</h1>} />
            </Switch>
        </Layout>
    );
};

export default Routes;
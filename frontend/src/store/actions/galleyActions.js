import {
    POST_NEW_PHOTO_ERROR,
    GET_GALLERY_SUCCESS,
    GET_GALLERY_ERROR
} from "../actionTypes";
import axiosBase from "../../axiosBase";
import {toast} from "react-toastify";


const getGallerySuccess = data => ({type: GET_GALLERY_SUCCESS, data});
const getGalleryError = error => ({type: GET_GALLERY_ERROR, error});
const postNewPhotoError = error => ({type: POST_NEW_PHOTO_ERROR, error})

export const getGallery = () => {

    return async dispatch => {
        try {
            let response = await axiosBase.get(`/gallery`);
            console.log(response.data, "datadatadatadatadatadatadatadatadatadata");
            dispatch(getGallerySuccess(response.data));
        } catch (e) {
            if(e.message === 'Network Error') {
                dispatch(getGalleryError(e.message));
            } else {
                dispatch(getGalleryError(e.response.data.error));
            }
        }
    };
};

export const postNewPhoto = (data,id) => {
    console.log(data,"data");
    let response;
    return async dispatch => {
        try {
            response = await axiosBase.post(`/gallery/new/${id}`, data)
            // dispatch(createArticleSuccess(response.data));
            toast.success(response.data.message);
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(postNewPhotoError(e.response.data));
            } else {
                dispatch(postNewPhotoError(e.message));
            }
        }
    };
};


import {
    GET_PLACES_SUCCESS,
    GET_PLACES_ERROR,
    DELETE_PLACE_SUCCESS,
    DELETE_PLACE_ERROR,
    POST_NEW_PLACE_ERROR,
    GET_FULL_PlACE_INFO_SUCCESS,
    GET_FULL_PlACE_INFO_ERROR
} from "../actionTypes";
import axiosBase from "../../axiosBase";
import {toast} from "react-toastify";
import {push} from "connected-react-router";

const getPlacesSuccess = data => ({type: GET_PLACES_SUCCESS, data});
const getPlacesError = error => ({type: GET_PLACES_ERROR, error});
const deletePlaceSuccess = id => ({type: DELETE_PLACE_SUCCESS,id});
const deletePlaceError = error => ({type: DELETE_PLACE_ERROR, error});
const getFullPlaceInfoSuccess = data => ({type: GET_FULL_PlACE_INFO_SUCCESS, data});
const getFullPlaceInfoError = data => ({type: GET_FULL_PlACE_INFO_ERROR, data});
const postNewPlaceError = error => ({type: POST_NEW_PLACE_ERROR, error})

export const getPlaces = () => {

    return async dispatch => {
        try {
            let response = await axiosBase.get(`/places`);
            dispatch(getPlacesSuccess(response.data));
        } catch (e) {
            if(e.message === 'Network Error') {
                dispatch(getPlacesError(e.message));
            } else {
                dispatch(getPlacesError(e.response.data.error));
            }
        }
    };
};
//
export const getFullPlaceInfo = (id)=> {
    return async dispatch => {
        try {
            const response = await axiosBase.get(`/places/${id}`);
            console.log(response ,"response");
            dispatch(getFullPlaceInfoSuccess(response.data));

        } catch (e) {
            if(e.message === 'Network Error') {
                dispatch(getFullPlaceInfoError(e.message));
            } else {
                dispatch(getFullPlaceInfoError(e.response.data.error));
            }
        }
    };
};

export const postNewPlace = (data) => {
    console.log(data);
    let response;
    return async dispatch => {
        try {
            response = await axiosBase.post(`/places/new`, data)
            toast.success(response.data.message);
            dispatch(push('/'));
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(postNewPlaceError(e.response.data));
            } else {
                dispatch(postNewPlaceError(e.message));
            }
        }
    };
};

export const deletePlace = id => {
    return async dispatch => {
        try {
            const response = await axiosBase.delete(`/places/${id}`);
            toast.success(response.data.message);
            dispatch(deletePlaceSuccess(id));
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(deletePlaceError(e.response.data.error));
            } else {
                dispatch(deletePlaceError(e.message));
            }
        }
    };
};


import {
    GET_REVIEWS_SUCCESS,
    GET_REVIEWS_ERROR,
    POST_NEW_REVIEW_SUCCESS,
    POST_NEW_REVIEW_ERROR,
    DELETE_REVIEW_SUCCESS,
    DELETE_REVIEW_ERROR
} from "../actionTypes";
import axiosBase from "../../axiosBase";
import {toast} from "react-toastify";

const getReviewsSuccess = data => ({type: GET_REVIEWS_SUCCESS, data});
const getReviewsError = error => ({type: GET_REVIEWS_ERROR, error});
const postNewReviewSuccess = () => ({type: POST_NEW_REVIEW_SUCCESS});
const postNewReviewError = error => ({type: POST_NEW_REVIEW_ERROR, error});
const deleteReviewSuccess = id => ({type: DELETE_REVIEW_SUCCESS,id});
const deleteReviewError = error => ({type: DELETE_REVIEW_ERROR, error});

export const getReviews = (id) => {
    return async dispatch => {
        try {
            const response = await axiosBase.get(`/reviews/${id}`);
            dispatch(getReviewsSuccess(response.data));
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(getReviewsError(e.response.data.error));
            } else {
                dispatch(getReviewsError(e.message));
            }
        }
    };
};

export const postNewReview = (data,id) => {
    return async dispatch => {
        try {
            const response = await axiosBase.post(`/reviews/${id}`, data);
            toast.success(response.data.message);
            dispatch(postNewReviewSuccess());
            dispatch(getReviews(id));
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(postNewReviewError(e.response.data));
            } else {
                dispatch(postNewReviewError(e.message));
            }
        }
    };
};

export const deleteReview = (id) => {
    return async dispatch => {
        try {
            const response = await axiosBase.delete(`/reviews/${id}`);
            toast.success(response.data.message);
            dispatch(deleteReviewSuccess(id));
        } catch (e) {
            if(e.response && e.response.data) {
                dispatch(deleteReviewError(e.response.data.error));
                toast.success(e.response.data.error);
            } else {
                dispatch(deleteReviewError(e.message));
            }
        }
    };
};


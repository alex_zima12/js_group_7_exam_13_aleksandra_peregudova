import {    GET_GALLERY_SUCCESS,
    GET_GALLERY_ERROR

} from "../actionTypes";

const initialState = {
    gallery:[],
    galleryErrors: null
};

const galleryReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_GALLERY_SUCCESS:
            return {
                ...state,
                gallery: action.data,
                galleryErrors: null
            };
        case GET_GALLERY_ERROR:
            return {
                ...state,
                galleryErrors: action.error
            };

        // case POST_NEW_PLACE_ERROR:
        //     return {
        //         ...state,
        //         placesErrors: action.error
        //     };
        // case GET_FULL_PlACE_INFO_SUCCESS:
        //     return {
        //         ...state,
        //         place: action.data,
        //         placesErrors: null
        //     };
        // case GET_FULL_PlACE_INFO_ERROR:
        //     return {
        //         ...state,
        //         placesErrors: action.error
        //     };
        //
        //
        // case DELETE_PLACE_SUCCESS:
        //     const id = state.places.findIndex(p => p._id === action.id);
        //     if (id !== -1) {
        //         const newPlaces= [...state.places];
        //         newPlaces.splice(id, 1);
        //         return {...state, places : [...newPlaces]};
        //     } else {
        //         return state;
        //     }
        // case DELETE_PLACE_ERROR:
        //     return {
        //         ...state,
        //         placesErrors: action.error
        //     };

        default:
            return state
    }
};

export default galleryReducer;

import {GET_PLACES_ERROR,
        GET_PLACES_SUCCESS,
        DELETE_PLACE_SUCCESS,
        DELETE_PLACE_ERROR,
        POST_NEW_PLACE_ERROR,
    GET_FULL_PlACE_INFO_SUCCESS,
    GET_FULL_PlACE_INFO_ERROR,

} from "../actionTypes";

const initialState = {
    places:[],
    place: null,
    placesErrors: null
};

const placesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_PLACES_SUCCESS:
            return {
                ...state,
                places: action.data,
                placesErrors: null
            };
        case GET_PLACES_ERROR:
            return {
                ...state,
                placesErrors: action.error
            };

        case POST_NEW_PLACE_ERROR:
            return {
                ...state,
                placesErrors: action.error
            };
        case GET_FULL_PlACE_INFO_SUCCESS:
            return {
                ...state,
                place: action.data,
                placesErrors: null
            };
        case GET_FULL_PlACE_INFO_ERROR:
            return {
                ...state,
                placesErrors: action.error
            };


        case DELETE_PLACE_SUCCESS:
            const id = state.places.findIndex(p => p._id === action.id);
            if (id !== -1) {
                const newPlaces= [...state.places];
                newPlaces.splice(id, 1);
                return {...state, places : [...newPlaces]};
            } else {
                return state;
            }
        case DELETE_PLACE_ERROR:
            return {
                ...state,
                placesErrors: action.error
            };

        default:
            return state
    }
};

export default placesReducer;

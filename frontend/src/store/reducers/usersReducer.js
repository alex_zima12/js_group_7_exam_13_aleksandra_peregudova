import {
    LOGIN_USER_ERROR,
    LOGIN_USER_SUCCESS, LOGOUT_USER,
    REGISTER_USER_ERROR,
} from "../actionTypes";

const initialState = {
    user: null,
    usersError: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                user: action.data,
                usersError: null
            };
        case LOGOUT_USER:
            return {
                ...state,
                user: null,
                usersError: null
            };
        case LOGIN_USER_ERROR:
        case REGISTER_USER_ERROR:
        default:
            return state;
    }
};

export default usersReducer;
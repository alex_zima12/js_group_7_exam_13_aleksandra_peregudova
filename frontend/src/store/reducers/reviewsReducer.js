import {
    GET_REVIEWS_SUCCESS,
    GET_REVIEWS_ERROR,
    POST_NEW_REVIEW_SUCCESS,
    POST_NEW_REVIEW_ERROR,
    DELETE_REVIEW_SUCCESS,
    DELETE_REVIEW_ERROR
} from "../actionTypes";

const initialState = {
    reviews: [],
    reviewsError: null,
    // pagesComments:null,
    // limitComments: 30
};

const reviewsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_REVIEWS_SUCCESS:
            return {
                ...state,
                reviews: action.data,
                reviewsError: null,
            };
        case GET_REVIEWS_ERROR:
            return {
                ...state,
                reviewsError: action.error,
                reviews: null
            };
        case POST_NEW_REVIEW_SUCCESS:
            return {
                ...state,
                error: null
            };
        case POST_NEW_REVIEW_ERROR:
            return {
                ...state,
                reviewsError: action.error
            };
        case DELETE_REVIEW_SUCCESS:
            const id = state.reviews.findIndex(p => p._id === action.id);
            if (id !== -1) {
                const newReviews= [...state.reviews];
                newReviews.splice(id, 1);
                return {...state, reviews : [...newReviews]};
            } else {
                return state;
            }
        case DELETE_REVIEW_ERROR:
            return {
                ...state,
                reviewsError: action.error
            };
        default:
            return state;
    }
};

export default reviewsReducer;
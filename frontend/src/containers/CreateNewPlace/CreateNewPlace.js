import React, { useState} from "react";
import {
    postNewPlace,
} from "../../store/actions/placesActions";
import Alert from '@material-ui/lab/Alert';
import {useDispatch, useSelector} from "react-redux";
import NewPlaceForm from "../../components/Place/NewPlaceForm";
import {toast} from "react-toastify";

const CreateNewPlace = () => {

    const dispatch = useDispatch();
    const user = useSelector((state) => state.users.user);
    const {placesErrors} = useSelector(state => state.places);

    const [state, setState] = useState({
        name: "",
        description: "",
        image: "",
        agree: false,
    });

    let warning = <Alert severity="info"> Вводя данные в поля формы, Вы подтверждаете, что введенные Вами данные, могут быть размещены на данном сайте</Alert>;

    const submitFormHandler = (e) => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach((key) => {
            formData.append(key, state[key]);
        });
        if (state.agree) {
            dispatch(
                postNewPlace(
                    formData,
                    user,
                    state.agree
                )
            );
        } else {
            toast.success("Отзыв может быть опубликован только после подтверждения!");
        }
    };

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setState((prevState) => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = (e) => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState((prevState) => ({
            ...prevState,
            [name]: file,
        }));
    };

    const getFieldError = (fieldName) => {
        try {
            return placesErrors.errors[fieldName];
        } catch (e) {
            return null;
        }
    };

    const checkboxChangeHandler = () => {
        if (!state.agree) {
            setState((prevState) => {
                return {
                    ...prevState,
                    agree: true,
                };
            });
        } else {
            setState((prevState) => {
                return {
                    ...prevState,
                    agree: false,
                };
            });
        }
    };

    return (
        <NewPlaceForm
            state={state}
            user={user}
            fileChangeHandler={fileChangeHandler}
            inputChangeHandler={inputChangeHandler}
            submitFormHandler={submitFormHandler}
            agree={state.agree}
            checkboxChangeHandler={checkboxChangeHandler}
            getFieldError={getFieldError}
            warning={warning}
        />

    );
};

export default CreateNewPlace;
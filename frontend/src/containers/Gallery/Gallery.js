import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@material-ui/core";
import PhotoItem from "../../components/PhotoItem/PhotoItem";
import {getGallery} from "../../store/actions/galleyActions";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap',
        transform: 'translateZ(0)',
    },
}));

const Gallery = ({id}) => {
    const classes = useStyles();
    const {gallery} = useSelector(state => state.gallery);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getGallery(id));
    }, [dispatch, id]);

    return (
        <Grid container className={classes.root}>
            <GridList className={classes.gridList}>
                {gallery && gallery.map(photo => {
                    return <PhotoItem
                        image={photo.image}
                    />

                })}
            </GridList>
        </Grid>

    );
};

export default Gallery;
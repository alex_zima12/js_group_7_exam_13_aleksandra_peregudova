import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getReviews, postNewReview} from "../../store/actions/reviewsAction";
import {Container, Typography} from "@material-ui/core";
import 'react-toastify/dist/ReactToastify.css';
import Alert from '@material-ui/lab/Alert';
import PropTypes from "prop-types";
import ReviewForm from "../../components/Review/ReviewForm";
import ReviewItem from "../../components/Review/ReviewItem";

const Reviews = ({id}) => {
    const {reviews, reviewsError} = useSelector(state => state.reviews);
    const {user} = useSelector(state => state.users);
    const dispatch = useDispatch();

    const [review, setReview] = useState('');

    const [rating, setRating] = useState({
        quality: 0,
        service: 0,
        interior: 0
    });

    const [state, setState] = useState({
        isButtonDisabled: false
    });

    const [openEmoji, setOpenEmoji] = useState(false);

    const onEmojiClick = (event, emojiObject) => {
        const messageEmoji = review + ' ' + emojiObject.emoji;
        setReview(messageEmoji);
        setOpenEmoji(false);
    };

    useEffect(() => {
        dispatch(getReviews(id));
    }, [dispatch, id]);

    const getFieldError = fieldName => {
        try {
            return reviewsError.errors[fieldName].message;
        } catch (e) {
            return null;
        }
    };

    const onChangeField = e => {
        const value = e.target.value;
        setReview(value);
    };

    const onSubmittedForm = e => {
        e.preventDefault();
        setState(prevState => {
            return {
                ...prevState,
                isButtonDisabled: true
            }
        });
        const data = {
            place: id,
            text: review,
            rating
        };
        dispatch(postNewReview(data, id));
    };

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setRating((prevState) => {
            return {...prevState, [name]: value};
        });
    };


    setTimeout(() => setState({isButtonDisabled: false}), 20000);

    return (
        <Container>
            <>
                {user &&
                <ReviewForm
                    submit={e => onSubmittedForm(e)}
                    change={e => onChangeField(e)}
                    click={onEmojiClick}
                    getFieldError={getFieldError}
                    isDisabled={state.isButtonDisabled}
                    text={review}
                    rating={rating}
                    emoji={openEmoji}
                    setEmoji={setOpenEmoji}
                    inputChangeHandler={inputChangeHandler}
                />}

                <Typography
                    variant="h5"
                    align="center"
                >
                    Отзывы
                </Typography>
                <>
                    {reviews &&
                    reviews.map(review => <ReviewItem
                        key={review._id}
                        text={review.text}
                        date={review.date}
                        author={review.user.username}
                        authorId={review.user._id}
                        id={review._id}
                        quality={review.quality}
                        service={review.service}
                        interior={review.interior}
                    />)}
                    {reviewsError &&
                    <Alert severity="error">{reviewsError}</Alert>}
                </>
            </>
        </Container>
    );
};

Reviews.propTypes = {
    id: PropTypes.string.isRequired,
};

export default Reviews;
import React, { useState} from 'react';
import {Typography, Button, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../components/UI/FormElement/FormElement";
import { loginUser, registerUser} from "../store/actions/usersActions";

const useStyles = makeStyles(() => ({
    form: {
        maxWidth: '400px',
        margin: '0 auto'
    }
}));

const LoginRegister = (props) => {
    const classes = useStyles();
    const {usersError} = useSelector(state => state.users);
    const dispatch = useDispatch();
    const url = props.match.url;

    const [state, setState] = useState({
        username: '',
        password: '',
        displayName: ''
    });

    const inputChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setState((prevState) => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return usersError.errors[fieldName].message;
        } catch (e) {
            return null;
        }
    };

    const submitFormHandler = e => {
        e.preventDefault();
        if(url === '/login') {
            dispatch(loginUser({username: state.username, password: state.password}));
        } else {
            dispatch(registerUser({state}));
        }
        setState({
            username: '',
            password: '',
            displayName: ''
        });
    }

    return (
        <>
            <Typography
                variant='h6'
                component='h6'
                align='center'
                gutterBottom
            >
                {url === '/login' ? 'Вход' : 'Регистрация'}
            </Typography>
            <form
                className={classes.form}
                onSubmit={submitFormHandler}
            >
                <FormElement
                    label='Логин'
                    id='form-login'
                    name='username'
                    type='text'
                    C
                    value={state.username}
                    error={getFieldError('username')}
                    onChange={inputChangeHandler}
                    required
                    className={classes.place}
                />
                <FormElement
                    label='Пароль'
                    id='form-pass'
                    name='password'
                    type='password'
                    value={state.password}
                    error={getFieldError('password')}
                    onChange={inputChangeHandler}
                    required
                    className={classes.place}
                />
                {url === '/register' && <FormElement
                    label='Имя'
                    id='form-displayName'
                    name='displayName'
                    type='text'
                    value={state.displayName}
                    error={getFieldError('displayName')}
                    onChange={inputChangeHandler}
                    required
                />}
                <Button
                    fullWidth
                    color='primary'
                    variant='outlined'
                    type='submit'
                    id='form-login-register-btn'
                >
                    {url === '/register' ? 'Регистрация' : 'Вход'}
                </Button>
            </form>
        </>
    );
};

export default LoginRegister;